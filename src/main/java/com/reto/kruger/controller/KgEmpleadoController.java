package com.reto.kruger.controller;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.reto.kruger.dto.EmpleadoVacunaDTO;
import com.reto.kruger.modelo.KgEmpleado;
import com.reto.kruger.service.IKgEmpleadoService;


@RestController
@RequestMapping("/kgEmpleado")
public class KgEmpleadoController {
	
	SimpleDateFormat formatoFechaCompleta = new SimpleDateFormat("dd/MM/yyyy");
	
	@Autowired
	private IKgEmpleadoService kgEmpleadoService;
	
		

	@PreAuthorize("@authServiceImpl.tieneAcceso('listarEmpleado')")
	@GetMapping
	public ResponseEntity<List<KgEmpleado>> listar() throws Exception{
		List<KgEmpleado> lista = kgEmpleadoService.listar();
		return new ResponseEntity<List<KgEmpleado>>(lista, HttpStatus.OK);		
	}	
	
	@PreAuthorize("@authServiceImpl.tieneAcceso('listarPorCedula')")
	@GetMapping("/empleadoPorCedula/{empleadoPorCedula}")
	public ResponseEntity<EmpleadoVacunaDTO> findById(@RequestParam("cedula") String cedula) throws Exception{
		EmpleadoVacunaDTO empleadoVacuna= kgEmpleadoService.findByCedula(cedula);
				
		return new ResponseEntity<EmpleadoVacunaDTO>(empleadoVacuna, HttpStatus.OK);
	}		
	
	@PreAuthorize("@authServiceImpl.tieneAcceso('filtrarEstadoVacunacion')")	
	@GetMapping("/filtrarEstadoVacunacion/{filtrarEstadoVacunacion}")
	public ResponseEntity<List<KgEmpleado>> filtrarEstadoVacunacion(@RequestParam(value = "estadoVacuna") String estadoVacuna) throws Exception{
		List<KgEmpleado> lista = kgEmpleadoService.findByEstadoVacuna(estadoVacuna);
		return new ResponseEntity<List<KgEmpleado>>(lista, HttpStatus.OK);		
	}	
	
	
	
	@PreAuthorize("@authServiceImpl.tieneAcceso('registrarEmpleado')")
	@PostMapping
	public ResponseEntity<KgEmpleado> registrar(@RequestBody KgEmpleado p) throws Exception{
		p.setFechaNacimiento(formatoFechaCompleta.parse(p.getFechaNacStr()));

		KgEmpleado obj = kgEmpleadoService.registrar(p);
		kgEmpleadoService.crearUsuario(p);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
		return ResponseEntity.created(location).build();
	}		
	
	
	
	@PreAuthorize("@authServiceImpl.tieneAcceso('registrarEmpleado')")
	@PutMapping
	public ResponseEntity<KgEmpleado> modificar(@RequestBody KgEmpleado p) throws Exception{
		p.setFechaNacimiento(formatoFechaCompleta.parse(p.getFechaNacStr()));

		KgEmpleado obj = kgEmpleadoService.modificar(p);
		return new ResponseEntity<KgEmpleado>(obj, HttpStatus.OK);
	}

	@PreAuthorize("@authServiceImpl.tieneAcceso('eliminarEmpleado')")
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception{
		kgEmpleadoService.eliminar(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}		
		

}
