package com.reto.kruger.dto;

import java.util.List;

import com.reto.kruger.modelo.KgEmpleado;
import com.reto.kruger.modelo.KgVacuna;

public class EmpleadoVacunaDTO {
	KgEmpleado empleado;
	List<KgVacuna>vacunas;
	
	public KgEmpleado getEmpleado() {
		return empleado;
	}
	public void setEmpleado(KgEmpleado empleado) {
		this.empleado = empleado;
	}
	public List<KgVacuna> getVacunas() {
		return vacunas;
	}
	public void setVacunas(List<KgVacuna> vacunas) {
		this.vacunas = vacunas;
	}
	
	
}
