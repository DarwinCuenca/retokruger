package com.reto.kruger.service;

import java.util.List;

import com.reto.kruger.dto.EmpleadoVacunaDTO;
import com.reto.kruger.modelo.KgEmpleado;
import com.reto.kruger.modelo.KgUsuario;


public interface IKgEmpleadoService extends ICRUD<KgEmpleado, Integer>{
	List<KgEmpleado>findAllEmpleados();	
	KgUsuario crearUsuario(KgEmpleado empleado);
	List<KgEmpleado>findByEstadoVacuna(String estadoVacuna);
	
	EmpleadoVacunaDTO findByCedula(String cedula);
	
	
}
