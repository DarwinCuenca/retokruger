package com.reto.kruger.service.impl;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.reto.kruger.dto.EmpleadoVacunaDTO;
import com.reto.kruger.modelo.KgEmpleado;
import com.reto.kruger.modelo.KgUsuario;
import com.reto.kruger.modelo.KgVacuna;
import com.reto.kruger.repo.IGenericRepo;
import com.reto.kruger.repo.IKgEmpleadoRepo;
import com.reto.kruger.repo.IKgRolRepo;
import com.reto.kruger.repo.IKgUsuarioRepo;
import com.reto.kruger.repo.IKgVacunaRepo;
import com.reto.kruger.service.IKgEmpleadoService;
import com.reto.kruger.modelo.KgEmpleado;
 


@Service
public class KgEmpleadoServiceImp extends CRUDImpl<KgEmpleado, Integer> implements IKgEmpleadoService{
	
	@Autowired
	private IKgEmpleadoRepo repo;

	@Autowired
	private IKgUsuarioRepo usuarioRepo;
	
	@Autowired
	private IKgRolRepo rolRepo;

	@Autowired
	private IKgVacunaRepo vacunaRepo;
	
	@Autowired
	private BCryptPasswordEncoder bcrypt;	

	@Override
	protected IGenericRepo<KgEmpleado, Integer> getRepo() {
		// TODO Auto-generated method stub
		return repo;
	}


	@Override
	public List<KgEmpleado> findAllEmpleados() {
		return repo.findAll();
	}


	@Override
	public KgUsuario crearUsuario(KgEmpleado empleado) {
		// TODO Auto-generated method stub
		KgUsuario nuevoUsuario=new KgUsuario();
		String partesApellidos[]=empleado.getApellidos().split(" ");
		String partesNombres[]=empleado.getNombres().split(" ");
		
		String usuario=partesApellidos[0].trim()+""+partesNombres[0].substring(0);
		
		
		nuevoUsuario.setUsuario(usuario);
		nuevoUsuario.setIdEmpleado(empleado);
		nuevoUsuario.setIdRol(rolRepo.findByRolNombre(empleado.getNombreRol()));
		nuevoUsuario.setContrasenia(usuario);
		nuevoUsuario.setEstado("A");
		
		String pass=partesApellidos[0].substring(0)+"08"+partesNombres[0].substring(0)+"01";
		nuevoUsuario.setContrasenia(bcrypt.encode(pass));		
		
		
		usuarioRepo.save(nuevoUsuario);
		return nuevoUsuario;
	}


	@Override
	public List<KgEmpleado> findByEstadoVacuna(String estadoVacuna) {
		// TODO Auto-generated method stub
		return repo.findByEstadoVacuna(estadoVacuna);
	}


	@Override
	public EmpleadoVacunaDTO findByCedula(String cedula) {
		// TODO Auto-generated method stub
		KgEmpleado empleado= repo.findByCedula(cedula);
		List<KgVacuna>vacunas=new ArrayList<>();
		if(empleado.getEstadoVacuna().equals("Vacunado")) {
			vacunas=vacunaRepo.findByIdEmpleado(empleado);
		}
		EmpleadoVacunaDTO empVac=new EmpleadoVacunaDTO();
		empVac.setEmpleado(empleado);
		empVac.setVacunas(vacunas);
		return empVac;
	}
		
	


	
}
