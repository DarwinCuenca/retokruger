package com.reto.kruger.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.userdetails.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.reto.kruger.modelo.KgEmpleado;
import com.reto.kruger.modelo.KgUsuario;
import com.reto.kruger.repo.IGenericRepo;
import com.reto.kruger.repo.IKgRolRepo;
import com.reto.kruger.repo.IKgUsuarioRepo;
 


@Service
public class KgUsuarioServiceImp implements UserDetailsService {
	
	@Autowired
	private IKgUsuarioRepo usuarioRepo;


	@Autowired
	private IKgRolRepo rolRepo;
	
	@Autowired
	private BCryptPasswordEncoder bcrypt;	
	

	public void crearUsuario(KgEmpleado empleado) {
		// TODO Auto-generated method stub
		KgUsuario nuevoUsuario=new KgUsuario();
		String partesApellidos[]=empleado.getApellidos().split(" ");
		String partesNombres[]=empleado.getNombres().split(" ");
		
		String usuario=partesApellidos[0].trim()+""+partesNombres[0].substring(0);
		
		
		nuevoUsuario.setUsuario(usuario);
		nuevoUsuario.setIdEmpleado(empleado);
		nuevoUsuario.setIdRol(rolRepo.findByRolNombre(empleado.getNombreRol()));
		nuevoUsuario.setContrasenia(usuario);
		nuevoUsuario.setEstado("A");
		
		String pass=partesApellidos[0].substring(0)+"08"+partesNombres[0].substring(0)+"01";
		nuevoUsuario.setContrasenia(bcrypt.encode(pass));		
		
		
		usuarioRepo.save(nuevoUsuario);
		
		
	}


	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		
		KgUsuario usuario = usuarioRepo.findByUsrNombreAplicacion(username);		
		
		if(usuario == null) {
			throw new UsernameNotFoundException(String.format("Usuario no existe", username));
		}
		
		List<GrantedAuthority> roles = new ArrayList<>();
		System.out.println("El rol a asignar es "+usuario.getIdRol().getRolNombre());
		roles.add(new SimpleGrantedAuthority(usuario.getIdRol().getRolNombre()));
		//roles.add(new SimpleGrantedAuthority(usuario.getCodEmpleado());
		boolean estado;
		if(usuario.getEstado().equals("A")) {
			estado=true;
		}else {
			estado = false;
		}
		UserDetails ud = new User(usuario.getUsuario(), usuario.getContrasenia() , estado, true, true, true, roles);
		
		return ud;
	}
	
	
}
