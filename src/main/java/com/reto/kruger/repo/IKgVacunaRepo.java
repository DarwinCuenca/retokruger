package com.reto.kruger.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.reto.kruger.modelo.KgEmpleado;
import com.reto.kruger.modelo.KgVacuna;


public interface IKgVacunaRepo extends IGenericRepo<KgVacuna, Integer> {
    @Query("FROM KgVacuna c WHERE c.idEmpleado = :idEmpleado")	
	List<KgVacuna> findByIdEmpleado(@Param("idEmpleado")KgEmpleado idEmpleado);		
	
}
