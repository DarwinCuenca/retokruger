package com.reto.kruger.repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.reto.kruger.modelo.KgUsuario;


public interface IKgUsuarioRepo extends IGenericRepo<KgUsuario, Integer> {
	@Query("FROM KgUsuario t WHERE t.usuario = :usrNombre ")
	KgUsuario findByUsrNombreAplicacion(@Param("usrNombre")String usrNombre);	
	
}
