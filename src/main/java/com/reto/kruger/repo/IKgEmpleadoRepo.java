package com.reto.kruger.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.reto.kruger.modelo.KgEmpleado;


public interface IKgEmpleadoRepo extends IGenericRepo<KgEmpleado, Integer> {
	
    @Query("FROM KgEmpleado c WHERE c.estadoVacuna = :estadoVacuna")	
	List<KgEmpleado> findByEstadoVacuna(@Param("estadoVacuna")String estadoVacuna);	
    
    @Query("FROM KgEmpleado c WHERE c.cedula = :cedula")	
	KgEmpleado findByCedula(@Param("cedula")String cedula);	
}
