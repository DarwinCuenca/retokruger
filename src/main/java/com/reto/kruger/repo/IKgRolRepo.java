package com.reto.kruger.repo;

import com.reto.kruger.modelo.KgRol;


public interface IKgRolRepo extends IGenericRepo<KgRol, Integer> {
	KgRol findByRolNombre(String rolNombre);
	
}
