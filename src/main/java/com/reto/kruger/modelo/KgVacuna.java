package com.reto.kruger.modelo;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "KG_VACUNA")
@TableGenerator(
        name = "GeneradorKgVacuna",
        table = "Generador",
        pkColumnName = "nombre",
        valueColumnName = "valor",        
        initialValue = 1,
        allocationSize = 1)
public class KgVacuna {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "GeneradorKgVacuna")
    @Column(name = "ID")	
    private Integer id;
    
    @Column(name = "TIPOVACUNA")
    private String tipoVacuna;
    
    @Column(name = "FECHAVACUNACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaVacunacion;
    
    @Column(name = "NUMERODOSIS")    
    private Integer numeroDosis;        

    @JoinColumn(name = "IDEMPLEADO", referencedColumnName = "ID")
    @ManyToOne
    private KgEmpleado idEmpleado;
    
    
    public KgVacuna() {
    	
    }

	public KgVacuna(String tipoVacuna, Date fechaVacunacion, Integer numeroDosis, KgEmpleado idEmpleado) {
		super();
		this.tipoVacuna = tipoVacuna;
		this.fechaVacunacion = fechaVacunacion;
		this.numeroDosis = numeroDosis;
		this.idEmpleado = idEmpleado;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTipoVacuna() {
		return tipoVacuna;
	}

	public void setTipoVacuna(String tipoVacuna) {
		this.tipoVacuna = tipoVacuna;
	}

	public Date getFechaVacunacion() {
		return fechaVacunacion;
	}

	public void setFechaVacunacion(Date fechaVacunacion) {
		this.fechaVacunacion = fechaVacunacion;
	}

	public Integer getNumeroDosis() {
		return numeroDosis;
	}

	public void setNumeroDosis(Integer numeroDosis) {
		this.numeroDosis = numeroDosis;
	}

	public KgEmpleado getIdEmpleado() {
		return idEmpleado;
	}

	public void setIdEmpleado(KgEmpleado idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	@Override
	public String toString() {
		return "KgVacuna [id=" + id + ", tipoVacuna=" + tipoVacuna + ", fechaVacunacion=" + fechaVacunacion
				+ ", numeroDosis=" + numeroDosis + ", idEmpleado=" + idEmpleado + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KgVacuna other = (KgVacuna) obj;
		return Objects.equals(id, other.id);
	}    
    
    
    

}
