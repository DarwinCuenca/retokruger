package com.reto.kruger.modelo;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.Size;


@Entity
@Table(name = "KG_USUARIO")
@TableGenerator(
        name = "GeneradorKgUsuario",
        table = "Generador",
        pkColumnName = "nombre",
        valueColumnName = "valor",        
        initialValue = 1,
        allocationSize = 1)
public class KgUsuario {
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
	int id;
    
    @Column(name = "USUARIO")
    private String usuario;  
    
    @Column(name = "CONTRASENIA")
    private String contrasenia;
    
    @Size(max = 1)
    @Column(name = "ESTADO")
    private String estado;    
    

    @JoinColumn(name = "IDEMPLEADO", referencedColumnName = "ID")
    @ManyToOne
    private KgEmpleado idEmpleado; 
    
    @JoinColumn(name = "IDROL", referencedColumnName = "ID")
    @ManyToOne
    private KgRol idRol;

    
    public KgUsuario() {    	
    }
    
	public KgUsuario(String usuario, String contrasenia, @Size(max = 1) String estado, KgEmpleado idEmpleado,
			KgRol idRol) {
		super();
		this.usuario = usuario;
		this.contrasenia = contrasenia;
		this.estado = estado;
		this.idEmpleado = idEmpleado;
		this.idRol = idRol;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getContrasenia() {
		return contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public KgEmpleado getIdEmpleado() {
		return idEmpleado;
	}

	public void setIdEmpleado(KgEmpleado idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public KgRol getIdRol() {
		return idRol;
	}

	public void setIdRol(KgRol idRol) {
		this.idRol = idRol;
	}    
    
    
    
    
    
}
