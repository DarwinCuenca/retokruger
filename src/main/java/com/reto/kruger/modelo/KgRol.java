/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.reto.kruger.modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

/**
 *
 * @author darwin
 */
@Entity
@Table(name = "KG_ROL")
/*@TableGenerator(
        name = "GeneradorKgRol",
        table = "Generador",
        pkColumnName = "nombre",
        valueColumnName = "valor",        
        initialValue = 1,
        allocationSize = 1)*/
@NamedQueries({
    @NamedQuery(name = "KgRol.findAll", query = "SELECT s FROM KgRol s")
    })
public class KgRol {   
    @Id
    //@GeneratedValue(strategy = GenerationType.TABLE, generator = "GeneradorKgRol")
    @Column(name = "ID")
    private Integer id;
    
    @Column(name = "ROL_NOMBRE")
    private String rolNombre;
    
    @Column(name = "ESTADO")
    private String estado;

    public KgRol() {    	
    }
    
	public KgRol(String rolNombre, String estado) {
		super();
		this.rolNombre = rolNombre;
		this.estado = estado;
	}
    
	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public String getRolNombre() {
		return rolNombre;
	}



	public void setRolNombre(String rolNombre) {
		this.rolNombre = rolNombre;
	}



	public String getEstado() {
		return estado;
	}



	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "KgRol [id=" + id + ", rolNombre=" + rolNombre + ", estado=" + estado + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, rolNombre);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KgRol other = (KgRol) obj;
		return Objects.equals(id, other.id) && Objects.equals(rolNombre, other.rolNombre);
	}




    


        
}
