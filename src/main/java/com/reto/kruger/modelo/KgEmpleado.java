package com.reto.kruger.modelo;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name = "KG_EMPLEADO")
@TableGenerator(
        name = "GeneradorKgEmpleado",
        table = "Generador",
        pkColumnName = "nombre",
        valueColumnName = "valor",        
        initialValue = 10,
        allocationSize = 1)

@NamedQueries({
    @NamedQuery(name = "KgEmpleado.findAll", query = "SELECT s FROM KgEmpleado s"),
    })
public class KgEmpleado {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "GeneradorKgEmpleado")
    @Column(name = "ID")
	int id;
    
    @Size( max = 10, message="Campo Cedula debe tener que tener un valor de 10 caracteres")
    @Pattern(regexp = "[0-9]+", message="Campo Cedula solo puede tener números")
    @NotNull(message = "Campo Cedula es Obligatorio")
    @Column(name = "CEDULA")
    private String cedula;
	
    @NotNull(message = "Campo Nombres es Obligatorio")
    @Column(name = "NOMBRES")
    private String nombres;
    
    @NotNull(message = "Campo Apellidos es Obligatorio")
    @Column(name = "APELLIDOS")
    private String apellidos;
    
    @NotNull(message = "Campo email es Obligatorio")
    @Email
    @Column(name = "CORREOELECTRONICO")
    private String correoElectronico;

    @NotNull(message = "Campo Fecha Nacimient es Obligatorio")
    @Column(name = "FECHANACIMIENTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaNacimiento;    
    
    @NotNull(message = "Campo Direccion es Obligatorio")
    @Column(name = "DIRECCION")
    private String direccion;
    
    @NotNull(message = "Campo Celular es Obligatorio")
    @Column(name = "CELULAR")
    private String celular;

    @NotNull(message = "Campo Estado Vacuna es Obligatorio")
    @Column(name = "ESTADOVACUNA")
    private String estadoVacuna;    
    
    @Transient
    String nombreRol;
	@Transient
    String fechaNacStr;	
    
    public KgEmpleado() {
    	
    }
 

	public KgEmpleado(String cedula, String nombres, String apellidos, String correoElectronico, Date fechaNacimiento,
			String direccion, String celular) {
		super();
		this.cedula = cedula;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.correoElectronico = correoElectronico;
		this.fechaNacimiento = fechaNacimiento;
		this.direccion = direccion;
		this.celular = celular;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}    
    
    
	
	
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}


	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}


	public String getDireccion() {
		return direccion;
	}


	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	public String getCelular() {
		return celular;
	}


	public void setCelular(String celular) {
		this.celular = celular;
	}


	public String getNombreRol() {
		return nombreRol;
	}


	public void setNombreRol(String nombreRol) {
		this.nombreRol = nombreRol;
	}


	public String getEstadoVacuna() {
		return estadoVacuna;
	}


	public void setEstadoVacuna(String estadoVacuna) {
		this.estadoVacuna = estadoVacuna;
	}

	public String getFechaNacStr() {
		return fechaNacStr;
	}


	public void setFechaNacStr(String fechaNacStr) {
		this.fechaNacStr = fechaNacStr;
	}  	

	@Override
	public int hashCode() {
		return Objects.hash(cedula);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KgEmpleado other = (KgEmpleado) obj;
		return Objects.equals(cedula, other.cedula);
	}

	@Override
	public String toString() {
		return "KgEmpleado [id=" + id + ", cedula=" + cedula + ", nombres=" + nombres + ", apellidos=" + apellidos
				+ ", correoElectronico=" + correoElectronico + "]";
	}


  
    
}
