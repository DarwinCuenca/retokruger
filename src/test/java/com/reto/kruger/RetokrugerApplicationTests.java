package com.reto.kruger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDateTime;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.jdbc.Sql;

import com.reto.kruger.modelo.KgEmpleado;
import com.reto.kruger.modelo.KgRol;
import com.reto.kruger.modelo.KgUsuario;
import com.reto.kruger.repo.IKgEmpleadoRepo;
import com.reto.kruger.repo.IKgRolRepo;
import com.reto.kruger.repo.IKgUsuarioRepo;



@SpringBootTest
@Sql({"/dataPrimerEmpleado.sql","/dataRoles.sql"})
class RetokrugerApplicationTests {
	
	@Test
	void contextLoads() {
	}
	
	@Autowired
	private IKgEmpleadoRepo repo;
	
	
	/*TEST encargado de ingresar los roles a la BD-*/
	@Test	
	public void testCrearEmpleadoAdministrador() {
		assertEquals(2, repo.findAll().size());
	}

	/*TEST encargado de ingresar los roles a la BD*/
	@Test	
	public void testDataRoles() {
		assertEquals(2, repo.findAll().size());
	}	
	
	@Autowired
	private BCryptPasswordEncoder bcrypt;
	

	@Autowired
	private IKgUsuarioRepo usuarioRepo;
	
	/*TEST PRINCIPAL */
	@Test
	 void verificarClave() {
		/*ADMINISTRADOR */
		KgUsuario us=new KgUsuario();
		KgRol rol=new KgRol();		
		KgEmpleado empleado = new KgEmpleado(); 
		rol.setId(1);
		empleado.setId(1);			
		us.setId(1);				
		us.setEstado("A");
		us.setUsuario("cuencad");		
		us.setContrasenia(bcrypt.encode("1234"));		
		us.setIdRol(rol);
		us.setIdEmpleado(empleado);
		
		usuarioRepo.save(us);
		
		/*EMPLEADO */
		us=new KgUsuario();
		rol=new KgRol();		
		empleado = new KgEmpleado(); 
		rol.setId(2);
		empleado.setId(2);			
		us.setId(2);				
		us.setEstado("A");
		us.setUsuario("pardoc");		
		us.setContrasenia(bcrypt.encode("1234"));		
		us.setIdRol(rol);
		us.setIdEmpleado(empleado);
		
		KgUsuario retorno=usuarioRepo.save(us);		
		assertTrue(retorno.getContrasenia().equals(us.getContrasenia()));
					
	}	
}
